package AufgabenSchleifen;

public class Folgen {

    public static void main(String[] args) {

        // x = Endwert
        int a = 99;
        int ax = 9;
        int b = 1;
        int bx = 400;
        int c = 2;
        int cx = 102;
        int d = 4;
        int dx = 1024;
        int e = 2;
        int ex = 32768;

        System.out.print("\na) ");
        while (true){
            System.out.print(a);
            if (a > ax) {
                System.out.print(", ");
            }
            a = a - 3;
            if (a < ax) break;
        }

        int cc = 3;
        System.out.print("\nb) ");
        while (true){
            System.out.print(b);
            if (b < bx) {
                System.out.print(", ");
            }
            b = b + cc;
            cc = cc + 2;
            if (b > bx) break;
        }

        System.out.print("\nc) ");
        while (true){
            System.out.print(c);
            if (c < cx) {
                System.out.print(", ");
            }
            c = c + 4;
            if (c > cx) break;
        }

        System.out.print("\nd) ");
        while (true){
            System.out.print(d);
            if (d < dx) {
                System.out.print(", ");
            }
            d = d + 12;
            if (d > dx) break;
        }

        System.out.print("\ne) ");
        while (true) {
            System.out.print(e);
            if (e < ex) {
                System.out.print(", ");
            }
            e = e * 2;
            if (e > ex) break;
        }
    }
}
