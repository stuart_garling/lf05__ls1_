package AufgabenSchleifen;
import java.util.Scanner;

public class Sterne {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine Zahle ein.");
        int zahl = sc.nextInt();
        String stern = "*";

        while (zahl > 0 ) {
            System.out.println(stern);
            zahl--;
            stern = stern + "*";
        }
    }
}
