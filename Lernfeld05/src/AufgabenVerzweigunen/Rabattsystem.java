package AufgabenVerzweigunen;

import java.util.Scanner;

public class Rabattsystem {

    public static void main(String[] args) {

        System.out.println("Wie ist der Betrag?");

        Scanner in = new Scanner(System.in);
        double bruh = in.nextDouble();
        in.close();

        if(bruh > 0 && bruh < 100) {
            bruh = bruh - (bruh * 0.10);
            System.out.println("Sie haben 10% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + bruh + " �");
        }
        else if(bruh > 100 && bruh < 500) {
            bruh = bruh - (bruh * 0.15);
            System.out.println("Sie haben 15% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + bruh + " �");
        }
        else if(bruh > 500) {
            bruh = bruh - (bruh * 0.20);
            System.out.println("Sie haben 20% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + bruh + " �");
        }
    }
}
