package Fahrkartenautomat;

import java.util.Scanner;

class Fahrkartenautomat {
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {

        while (tastatur != null) {
            double zuZahlenderBegetrag = fahrkartenbestellungErfassen(), ruckgabebetrag = fahrkartenBezahlen(zuZahlenderBegetrag);

            fahrkartenAusgeben();

            rueckgeldAusgeben(ruckgabebetrag);

            try {
                Thread.sleep(1000);
            } catch (Exception ignore) {}

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir w�nschen Ihnen eine gute Fahrt.\n\n\n");
        }

        tastatur.close();
    }

    public static double fahrkartenbestellungErfassen() {
        int gewaehltesTicket;
        double gesamtPreis = 0;
        int nochEinTicketInt;
        String[] zwischenspeicher = new String [10];
        int anzahlVerfuegbareTickets = 10;
        int v = 1;
        int x = 1;


        int fahrkartenNummer[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String[] fahrkartenBezeichnung = {"EinzelfahrscheinBerlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke",
                "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };
        double[] fahrkartenPreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

        String aa = "Auswahlnummer";
        String bb = "Bezeichnung";
        String cc = "Preis in Euro";

        System.out.printf("\n|%-15s", aa);
        System.out.printf("|%-36s|", bb);
        System.out.printf("%-15s|\n", cc);
        System.out.printf("|---------------|------------------------------------|---------------|\n");

        System.out.printf("|%-15s", fahrkartenNummer[0]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[0]);
        System.out.printf("%15s|\n", fahrkartenPreise[0] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[1]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[1]);
        System.out.printf("%15s|\n", fahrkartenPreise[1] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[2]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[2]);
        System.out.printf("%15s|\n", fahrkartenPreise[2] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[3]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[3]);
        System.out.printf("%15s|\n", fahrkartenPreise[3] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[4]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[4]);
        System.out.printf("%15s|\n", fahrkartenPreise[4] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[5]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[5]);
        System.out.printf("%15s|\n", fahrkartenPreise[5] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[6]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[6]);
        System.out.printf("%15s|\n", fahrkartenPreise[6] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[7]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[7]);
        System.out.printf("%15s|\n", fahrkartenPreise[7] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[8]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[8]);
        System.out.printf("%15s|\n", fahrkartenPreise[8] + "0 �");

        System.out.printf("|%-15s", fahrkartenNummer[9]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[9]);
        System.out.printf("%15s|\n", fahrkartenPreise[9] + "�");

        System.out.println("\nSie k�nnen noch Maximal " + anzahlVerfuegbareTickets + " Tickets kaufen");
        System.out.println("Bitte w�hlen Sie ein Ticket.");

        gewaehltesTicket = tastatur.nextInt();

        if (gewaehltesTicket == 1) {
            gesamtPreis = fahrkartenPreise[0] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [0];
        }
        else if (gewaehltesTicket == 2) {
            gesamtPreis = fahrkartenPreise[1] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [1];
        }
        else if (gewaehltesTicket == 3) {
            gesamtPreis = fahrkartenPreise[2] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [2];
        }
        else if (gewaehltesTicket == 4) {
            gesamtPreis = fahrkartenPreise[3] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [3];
        }
        else if (gewaehltesTicket == 5) {
            gesamtPreis = fahrkartenPreise[4] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [4];
        }
        else if (gewaehltesTicket == 6) {
            gesamtPreis = fahrkartenPreise[5] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [5];
        }
        else if (gewaehltesTicket == 7) {
            gesamtPreis = fahrkartenPreise[6] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [6];
        }
        else if (gewaehltesTicket == 8) {
            gesamtPreis = fahrkartenPreise[7] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [7];
        }
        else if (gewaehltesTicket == 9) {
            gesamtPreis = fahrkartenPreise[8] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [8];
        }
        else if(gewaehltesTicket == 10) {
            gesamtPreis = fahrkartenPreise[9] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [9];
        }

        x++;

        anzahlVerfuegbareTickets = anzahlVerfuegbareTickets - 1;
        System.out.println("Der jetzige Gesamtpreis betr�gt " + gesamtPreis + "0 �.");

        for ( int i = 0; i < 9; i++) {

            System.out.println("\n\nSie k�nnen noch Maximal " + anzahlVerfuegbareTickets + " Tickets kaufen");
            System.out.println("\nM�chten Sie noch ein Ticket kaufen?\nJa = 1\nNein = 0");
            nochEinTicketInt = tastatur.nextInt();

            if (nochEinTicketInt == 1) {

                System.out.println("\nBitte w�hlen Sie noch ein Ticket.\n");

                gewaehltesTicket = tastatur.nextInt();

                if (gewaehltesTicket == 1) {
                    gesamtPreis = fahrkartenPreise[0] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [0];
                }
                else if (gewaehltesTicket == 2) {
                    gesamtPreis = fahrkartenPreise[1] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [1];
                }
                else if (gewaehltesTicket == 3) {
                    gesamtPreis = fahrkartenPreise[2] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [2];
                }
                else if (gewaehltesTicket == 4) {
                    gesamtPreis = fahrkartenPreise[3] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [3];
                }
                else if (gewaehltesTicket == 5) {
                    gesamtPreis = fahrkartenPreise[4] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [4];
                }
                else if (gewaehltesTicket == 6) {
                    gesamtPreis = fahrkartenPreise[5] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [5];
                }
                else if (gewaehltesTicket == 7) {
                    gesamtPreis = fahrkartenPreise[6] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [6];
                }
                else if (gewaehltesTicket == 8) {
                    gesamtPreis = fahrkartenPreise[7] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [7];
                }
                else if (gewaehltesTicket == 9) {
                    gesamtPreis = fahrkartenPreise[8] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [8];
                }
                else if(gewaehltesTicket == 10) {
                    gesamtPreis = fahrkartenPreise[9] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [9];
                }

                anzahlVerfuegbareTickets = anzahlVerfuegbareTickets - 1;
                System.out.println("\nDer Gesamtpreis betr�gt " + gesamtPreis + "0 �.");

                System.out.print("Sie haben folgene Tickets im Warenkorb: ");
                for (int f = 0; f < x; f++) {
                    System.out.print("\n" + zwischenspeicher[f]);
                }
                v++;
                x++;

            }
            else if (nochEinTicketInt >= 1 && nochEinTicketInt <= 0) {
                System.out.print("Sie haben einen ungl�ltigen Wert angegeben.");
            }
            else if (nochEinTicketInt == 0) {
                break;
            }
        }
        return gesamtPreis;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.println("Noch zu zahlen: " + String.format("%.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingezahlterGesamtbetrag += tastatur.nextDouble();
        }

        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double ruckgabebetrag) {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------

        if (ruckgabebetrag > 0.0) {
            System.out.println("Der R�ckgabebetrag in H�he von " + String.format("%.2f Euro", ruckgabebetrag));
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            int zweiEuro = 0, einEuro = 0, fuenfzigCent = 0, zwanzigCent = 0, zehnCent = 0, fuenfCent = 0;

            while (ruckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                zweiEuro++;
                ruckgabebetrag -= 2.0;
            }
            while (ruckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                einEuro++;
                ruckgabebetrag -= 1.0;
            }
            while (ruckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                fuenfzigCent++;
                ruckgabebetrag -= 0.5;
            }
            while (ruckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                zwanzigCent++;
                ruckgabebetrag -= 0.2;
            }
            while (ruckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                zehnCent++;
                ruckgabebetrag -= 0.1;
            }
            while (ruckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                fuenfCent++;
                ruckgabebetrag -= 0.05;
            }

            if (zweiEuro > 0) System.out.println(zweiEuro + " mal 2 Euro St�ck" + (zweiEuro > 1 ? "e" : "") + ".");
            if (einEuro > 0) System.out.println(einEuro + " mal 1 Euro St�ck" + (einEuro > 1 ? "e" : "") + ".");
            if (fuenfzigCent > 0) System.out.println(fuenfzigCent + " mal 50 Cent St�ck" + (fuenfzigCent > 1 ? "e" : "") + ".");
            if (zwanzigCent > 0) System.out.println(zwanzigCent + " mal 20 Cent St�ck" + (zwanzigCent > 1 ? "e" : "") + ".");
            if (zehnCent > 0) System.out.println(zehnCent + " mal 10 Cent St�ck" + (zehnCent > 1 ? "e" : "") + ".");
            if (fuenfCent > 0) System.out.println(fuenfCent + " mal 5 Cent St�ck" + (fuenfCent > 1 ? "e" : "") + ".");

        }
    }
}
//Aufgabe 1

//Variable:					|	Datentyp:
//zuZahlenderBetrag			|	double
//eingezahlterGesamtbetrag	|	double
//eingeworfeneM�nze			|	double
//r�ckgabebetrag			|	double
//ticketpreis				|	double
//ticketanzahl				|	int


//Aufgabe 2

//Variablen:				|	Operatoren:
//zuZahlenderBetrag			|	scanner eingabe
//eingezahlterGesamtbetrag	|	0.0
//eingeworfeneM�nze			|	scanner eingabe
//r�ckgabebetrag			|	eingazahlterGesamtbetrag - zuZahlenderBetrag
//ticketpreis				|	2.50
//ticketanzahl				|	zuZahlenderBetrag / ticketpreis


//Aufgabe 5

//ticketpreis: 	double weil es eine sehr genaue Zahl sein muss 
//ticketanzahl:	int weil die Anzahl keine Kommastellen haben darf


//Aufgabe 6