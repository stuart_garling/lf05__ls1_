package Fahrkartenautomat;
import java.util.Scanner;

public class AbKonsolenausgabe {
	public static void main(String[]args) {
	
		Scanner in = new Scanner(System.in);
		
		// Aufgabe 1
		
		// Part 1:   Addition
		
		System.out.println("Addition:");
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");   	
		int summand1 = in.nextInt();
		
		System.out.println("Mit welcher Zahl m�chten Sie Addieren: ");
		int summand2 = in.nextInt();
		
		int ergebnis = summand1 + summand2;
		
		System.out.println("\nDie Summe der Addition lautet: ");
		System.out.println(summand1 + " + " + summand2 + " = " + ergebnis); 
		
		// Part 2: Subtraktion ein
		
		System.out.println("\nSubtraktion:");
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");   	
		int minuend = in.nextInt();
		
		System.out.println("Mit welcher Zahl wollen subtrahieren? ");
		int subtrahend= in.nextInt();
		
		int ergebnis2 = minuend - subtrahend;
		
		System.out.println("\nDie Summe der Subtraktion lautet: ");
		System.out.println(minuend + " - " + subtrahend + " = " + ergebnis2); 
		
		//	Part 3: Multiplikation
		
		System.out.println("\nMultiplikation:");
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		int Faktor1 = in.nextInt();
		
		System.out.println("Mit welcher Zahl m�chten Sie Multiplizieren? ");
		int Faktor2 = in.nextInt();
		
		int ergebnis3 = Faktor1 * Faktor2;
		
		System.out.println("\nDie Antwort lautetet: ");
		System.out.println(Faktor1 + " * " + Faktor2 + " = " + ergebnis3);
		
		// Part 4: Division
		
		System.out.println("\nDivision:");
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		int Dividend = in.nextInt();
		
		System.out.println("Durch welche Zahl m�chten Sie Teilen? ");
		int Divisor = in.nextInt();
		
		int ergebnis4 = Dividend / Divisor;
		
		System.out.println("\nDie Antwort lautet: ");
		System.out.println(Dividend + " / " + Divisor + " = " + ergebnis4);
		
		
		// Aufgabe 2
		
		String name;
		int alter;
		
		Scanner cs = new Scanner(System.in);
		
		System.out.println("Wie ist dein Name?");	
		name = cs.nextLine();
		
		System.out.println("Wie alt bist du?");
		alter = cs.nextInt();
		
		System.out.println("Du bist " + alter + " Jahre alt und du hei�t " + name + "." );
		
		
	}

}
