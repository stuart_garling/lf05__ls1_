package Fahrkartenautomat;

import java.util.Scanner;

class Fahrkartenautomatasd {
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {

        int gewähltesTicket;
        int nochEinTicketInt;
        double gesamtPreis = 0;
        String zwischenspeicher [] = new String [10];
        int anzahlVerfügbareTickets = 10;
        int v = 1;
        int x = 1;


        int fahrkartenNummer[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String fahrkartenBezeichnung[] = {"EinzelfahrscheinBerlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke",
                "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };
        double fahrkartenPreise[] = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

        String aa = "Auswahlnummer";
        String bb = "Bezeichnung";
        String cc = "Preis in Euro";

        System.out.printf("\n|%-15s", aa);
        System.out.printf("|%-36s|", bb);
        System.out.printf("%-15s|\n", cc);
        System.out.printf("|---------------|------------------------------------|---------------|\n");

        System.out.printf("|%-15s", fahrkartenNummer[0]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[0]);
        System.out.printf("%15s|\n", fahrkartenPreise[0] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[1]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[1]);
        System.out.printf("%15s|\n", fahrkartenPreise[1] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[2]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[2]);
        System.out.printf("%15s|\n", fahrkartenPreise[2] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[3]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[3]);
        System.out.printf("%15s|\n", fahrkartenPreise[3] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[4]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[4]);
        System.out.printf("%15s|\n", fahrkartenPreise[4] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[5]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[5]);
        System.out.printf("%15s|\n", fahrkartenPreise[5] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[6]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[6]);
        System.out.printf("%15s|\n", fahrkartenPreise[6] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[7]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[7]);
        System.out.printf("%15s|\n", fahrkartenPreise[7] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[8]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[8]);
        System.out.printf("%15s|\n", fahrkartenPreise[8] + "0 €");

        System.out.printf("|%-15s", fahrkartenNummer[9]);
        System.out.printf("|%-36s|", fahrkartenBezeichnung[9]);
        System.out.printf("%15s|\n", fahrkartenPreise[9] + "€");

        System.out.println("\nSie können noch Maximal " + anzahlVerfügbareTickets + " Tickets kaufen");
        System.out.println("Bitte wählen Sie ein Ticket.");

        gewähltesTicket = tastatur.nextInt();

        if (gewähltesTicket == 1) {
            gesamtPreis = fahrkartenPreise[0] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [0];
        }
        else if (gewähltesTicket == 2) {
            gesamtPreis = fahrkartenPreise[1] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [1];
        }
        else if (gewähltesTicket == 3) {
            gesamtPreis = fahrkartenPreise[2] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [2];
        }
        else if (gewähltesTicket == 4) {
            gesamtPreis = fahrkartenPreise[3] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [3];
        }
        else if (gewähltesTicket == 5) {
            gesamtPreis = fahrkartenPreise[4] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [4];
        }
        else if (gewähltesTicket == 6) {
            gesamtPreis = fahrkartenPreise[5] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [5];
        }
        else if (gewähltesTicket == 7) {
            gesamtPreis = fahrkartenPreise[6] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [6];
        }
        else if (gewähltesTicket == 8) {
            gesamtPreis = fahrkartenPreise[7] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [7];
        }
        else if (gewähltesTicket == 9) {
            gesamtPreis = fahrkartenPreise[8] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [8];
        }
        else if(gewähltesTicket == 10) {
            gesamtPreis = fahrkartenPreise[9] + gesamtPreis;
            zwischenspeicher [0] = fahrkartenBezeichnung [9];
        }

        x++;

        anzahlVerfügbareTickets = anzahlVerfügbareTickets - 1;
        System.out.println("Der jetzige Gesamtpreis beträgt " + gesamtPreis + "0 €.");

        for ( int i = 0; i < 9; i++) {

            System.out.println("\n\nSie können noch Maximal " + anzahlVerfügbareTickets + " Tickets kaufen");
            System.out.println("\nMöchten Sie noch ein Ticket kaufen?\nJa = 1\nNein = 0");
            nochEinTicketInt = tastatur.nextInt();

            if (nochEinTicketInt == 1) {

                System.out.println("\nBitte wählen Sie noch ein Ticket.\n");

                gewähltesTicket = tastatur.nextInt();

                if (gewähltesTicket == 1) {
                    gesamtPreis = fahrkartenPreise[0] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [0];
                }
                else if (gewähltesTicket == 2) {
                    gesamtPreis = fahrkartenPreise[1] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [1];
                }
                else if (gewähltesTicket == 3) {
                    gesamtPreis = fahrkartenPreise[2] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [2];
                }
                else if (gewähltesTicket == 4) {
                    gesamtPreis = fahrkartenPreise[3] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [3];
                }
                else if (gewähltesTicket == 5) {
                    gesamtPreis = fahrkartenPreise[4] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [4];
                }
                else if (gewähltesTicket == 6) {
                    gesamtPreis = fahrkartenPreise[5] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [5];
                }
                else if (gewähltesTicket == 7) {
                    gesamtPreis = fahrkartenPreise[6] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [6];
                }
                else if (gewähltesTicket == 8) {
                    gesamtPreis = fahrkartenPreise[7] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [7];
                }
                else if (gewähltesTicket == 9) {
                    gesamtPreis = fahrkartenPreise[8] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [8];
                }
                else if(gewähltesTicket == 10) {
                    gesamtPreis = fahrkartenPreise[9] + gesamtPreis;
                    zwischenspeicher [v] = fahrkartenBezeichnung [9];
                }

                anzahlVerfügbareTickets = anzahlVerfügbareTickets - 1;
                System.out.println("\nDer Gesamtpreis beträgt " + gesamtPreis + "0 €.");

                System.out.print("Sie haben folgene Tickets im Warenkorb: ");
                for (int f = 0; f < x; f++) {
                    System.out.print("\n" + zwischenspeicher[f]);
                }
                v++;
                x++;

            }
            else if (nochEinTicketInt >= 1 && nochEinTicketInt <= 0) {
                System.out.print("Sie haben einen unglültigen Wert angegeben.");
            }
            else if (nochEinTicketInt == 0) {
                break;
            }
        }
        //return gesamtPreis;

    }
}