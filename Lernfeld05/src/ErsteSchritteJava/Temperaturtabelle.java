package ErsteSchritteJava;

public class Temperaturtabelle {
	public static void main(String[]args) {

		String aa = "Fahrenheit";
		String bb = "Celsius";

		System.out.printf( "%-12s|", aa );
		System.out.printf( "%10s\n", bb );
		System.out.printf("-----------------------\n");

		String a = "-20";
		String b = "-28.8889";
		String c = "-10";
		String d = "-23.3333";
		String e = "+0";
		String f = "-17.7778";
		String g = "+20";
		String h = "-6.6667";
		String i = "+30";
		String j = "-1.1111";
		
		System.out.printf( "%-12s|", a );
		System.out.printf( "%10s\n", b );
		System.out.printf( "%-12s|", c );
		System.out.printf( "%10s\n", d );
		System.out.printf( "%-12s|", e );
		System.out.printf( "%10s\n", f );
		System.out.printf( "%-12s|", g );
		System.out.printf( "%10s\n", h );
		System.out.printf( "%-12s|", i );
		System.out.printf( "%10s\n", j );
	}
}
