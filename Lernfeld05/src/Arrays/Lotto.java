package Arrays;

public class Lotto {

    public static void main(String[] args) {

        int [] ziehung = {3, 7, 12, 18, 37, 42};

        System.out.print("[ ");

        for (int i = 0; i < ziehung.length; i++) {

            System.out.print(ziehung [i] + " ");

        } System.out.print("]");

        System.out.print("\n");

        for (int i = 0; i < ziehung.length; i++ ) {

            int b = ziehung[i];

            if (b == 12) {

                System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");

            }
        }

        for (int i = 0; i < ziehung.length; i++ ) {

            int b = ziehung[i];

            if (b == 13) {

                System.out.print("Die Zahl 13 ist in der Ziehung enthalten.");

            } else {

                System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");

            }
        }
    }
}
