package methoden;

import java.util.Scanner;

public class Aufgabe5Methoden {
	
	public static void main(String[] agrs) {
		
		Scanner sc = new Scanner(System.in);
		
		while (true) {
			System.out.println("\nWas soll umgerechnet weden?");
			String Eingabe;
			Eingabe = sc.nextLine();		
			
			if (Eingabe.endsWith("USD")) {
			double euro = USD(Double.parseDouble(Eingabe.replace("USD", "").replace(" ", "")));
			System.out.println("Sie m�ssen " + euro + "� bezahlen.");
			
			} 
			else if (Eingabe.endsWith("JPY")) {
			double euro = JPY(Double.parseDouble(Eingabe.replace("JPY", "").replace(" ", "")));
			System.out.println("Sie m�ssen " + euro + "� bezahlen.");
			
			} 
			else if (Eingabe.endsWith("GBP")) {
			double euro = GBP(Double.parseDouble(Eingabe.replace("GBP", "").replace(" ", "")));
			System.out.println("Sie m�ssen " + euro + "� bezahlen.");
			
			} 
			else if (Eingabe.endsWith("CHF")) {
			double euro = CHF(Double.parseDouble(Eingabe.replace("CHF", "").replace(" ", "")));
			System.out.println("Sie m�ssen " + euro + "� bezahlen.");
			
			} 
			else if (Eingabe.endsWith("SEK")) {
			double euro = SEK(Double.parseDouble(Eingabe.replace("SEK", "").replace(" ", "")));
			System.out.println("Sie m�ssen " + euro + "� bezahlen.");
			}
			
			
		}
		
	}

	public static double USD(double dollar) {
		return dollar * (1 / 1.22);
	}
	
	public static double JPY(double Yen) {
		return Yen * (1 / 126.50);
	}
	
	public static double GBP(double pfund) {
		return pfund * (1 / 0.89);
	}
	
	public static double CHF(double franken) {
		return franken * (1 / 1.08);
	}
	public static double SEK(double kronen) {
		return kronen * (1 / 10.10);
	}
}
