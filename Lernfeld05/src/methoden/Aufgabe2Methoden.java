package methoden;

public class Aufgabe2Methoden {

	public static void main(String[] args) {
		
		System.out.println(Aufgabe(2.36, 7.87));
	}
	
	public static double Aufgabe(double a, double b){
		return a*b;	
	}
}

//	Was ich denke:
//	1: Mana
//	2: Elise
//	3: Johanna
//	4: Felizitas
//	5: Karla
//	false
//	true
//	true
//
//	Antwort:
//	1: Mana
//	2: Elise
//	3: Johanna
//	4: Felizitas
//	5: Karla
//	false
//	true
//	true